#Notes on workflow

##Raw fastq files were provided without demultiplexing

```bash
   Boyson20180119_S1_L001_R1_001.fastq_.gz
   Boyson20180119_S1_L001_R2_001.fastq_.gz
```

##Files were "gunzipped"

```bash
    gunzip *.fastq
```

##Files were checked for quality using fastqc

```bash

    /gpfs1/mbsr_tools/Release2017-09-14/FastQC/fastqc *.fastq
```

After checking the files in Fastqc, it was noted that the reverse reads, "...R2_001.fastq" were superior in quality.
Reverse reads are usually poorer quality.
Also, read lengths were sometimes abnormally short, only 35 bp, which indicates that only the adapter was sequenced.

##Here are the links to the output reports:

["Forward" reads](https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/4c9b5b68b51748acdd4ac0991b8c1277c7641154/Boyson20180119_S1_L001_R1_001.fastq%20FastQC%20Report.pdf)
["Reverse" reads](https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/4c9b5b68b51748acdd4ac0991b8c1277c7641154/Boyson20180119_S1_L001_R2_001.fastq%20FastQC%20Report.pdf)

Based on poor quality near the end of the reads, the files were trimmed and clipped.

##Files were trimmed to remove sequences of base less than Q30 from the end, and minimum length of 100 after trimming

FASTX Toolkit 0.0.13 by A. Gordon (gordon@cshl.edu)
[FASTX Download site](http://hannonlab.cshl.edu/fastx_toolkit/)

```bash

    /gpfs1/mbsr_tools/Arhodes/FastX/bin/fastq_quality_trimmer -Q 33 -t 30 -l 100 -i Boyson20180119_S1_L001_R1_001.fastq -o Boyson20180119_S1_L001_R1_001.trimmed.fastq
    /gpfs1/mbsr_tools/Arhodes/FastX/bin/fastq_quality_trimmer -Q 33 -t 30 -l 100 -i Boyson20180119_S1_L001_R2_001.fastq -o Boyson20180119_S1_L001_R2_001.trimmed.fastq    

```

##Files were clipped to remove adapters, the trimming part was also redone starting with the Raw fastq files, using a sliding window approach this time, and sequences were grouped into paired and unpaired files.  This is helpful for the next step, pandaseq. The rerun of the trimming was mainly due to switching to a different tool "Trimmomatic" for adapter removal, which can be read about here: [Trimmomatic website](http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf)

```bash
    java -jar /gpfs1/mbsr_tools/Arhodes/Trimmomatic-0.36/trimmomatic-0.36.jar PE -threads 12 Boyson20180119_S1_L001_R1_001.fastq Boyson20180119_S1_L001_R2_001.fastq forward_paired.fq forward_unpaired.fq reverse_paired.fq reverse_unpaired.fq ILLUMINACLIP:All.both.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:50
```

Program output
```
    Input Read Pairs: 14030383 
    Both Surviving: 13818842 (98.49%) 
    Forward Only Surviving: 195813 (1.40%) 
    Reverse Only Surviving: 8413 (0.06%) 
    Dropped: 7315 (0.05%)
    TrimmomaticPE: Completed successfully
```
##Files were rechecked for quality using fastqc

```bash
    /gpfs1/mbsr_tools/Release2017-09-14/FastQC/fastqc -t 2 *_paired.fq
```

["Forward" reads](https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/0f1cc557dd712a2d73a560008bd19811c390753a/forward_paired.fq%20FastQC%20Report.pdf)
["Reverse" reads](https://bitbucket.org/arhodes7/mouse_lung_vg4_tcr/raw/0f1cc557dd712a2d73a560008bd19811c390753a/reverse_paired.fq%20FastQC%20Report.pdf)




##Pandaseq paired end assembler was used to combine the forward and reverse reads after trimming and clipping

[Paper to cite for pandaseq](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-13-31)

[Download instructions for pandaseq](https://github.com/neufeld/pandaseq/wiki/Installation)

```
    pandaseq -f forward_paired.fq -r reverse_paired.fq -T 12 -B -N -w combined.fa
```

##Number and length of reads checked after combining forward and reverse paired reads.


```bash

#Number of lines in forward paired reads

    grep "@M02" forward_paired.fq | wc -l
    13818842
    
#Number of lines in reverse paired reads

    grep "@M02" reverse_paired.fq | wc -l
    13818842

#Number of reads in combined file

    grep ">" combined.fa | wc -l
    13773226

#Length distribution of combined reads

    awk '/^>/ {print; next; } { seqlen = length($0); print seqlen}' combined.fa | grep -v ">" | sort | uniq -c | sort -nk2
    

#Read lengh between 50 and 500
#Trimodal distribution at 67,282 and 350
#In other words, the reads are all over the map, so pandaseq needs to be rerun with stricter parameters.

```

#Diagnosing the bimodal distribution of the combined.fa file

Two sequences were chosen at random from the combined.fa file

Sequence #1
________________

```

    >M02840:176:000000000-BLLHM:1:1101:14865:1357:1
    CCATTGGGACGAGGCCAGGGTTTTCCCAGTCACGACGTCGATTTTCTGTGAAGCACAGCAAGGCCAACAGAACCTTCCATCTGGTGATCTCTCCAGTGAGCCTTGAAGACAGCGCTACTTATTACTGTGCCTCGGGGTATATCGGAGGGATACGAGCTACCGACAAACTCGTCTTTGGACAAGGAACCCAAGTGACTGTGGAACCAAAAAGCCAGCCTCCGGCCAAACCATCTGTTTTCATCATGAAAAATGGAACAAATGTTGCTTGTCTGGTGTAAGTCA
```

This fits into the shorter range (282)

When blasted, it hit Mouse TCR.

[See attached picture](https://bytebucket.org/arhodes7/mouse_lung_vg4_tcr/raw/c63e5b6ae0b281485f342132cabdf68a3ea4ee6b/TCR_Blast.png?token=5a31099de9f4085a5ff64e2c7423bc0ee6dd7087)

Sequence #2 
_________________

```
    >M02840:176:000000000-BLLHM:1:1101:18032:1644:1
    TTGCGGCAAAACTGCGTAACCGTCTTCTCGTTCTCTAAAAACCATTTTTCGTCCCCTTCTGGGCGGTGGTCTATAGTGTTATTAATATCAAGTTGGGGGAGCACATTGTAGCATTGTGCCAATTCATCCATTAACTTCTCAGTAACAGATACAAACTCATCACGAACGTCAGAAGCAGCCTTATGGCCGTCAACATACATATCACCATTATCGAACTCAACGCCCTGCATACGAAAAGACAGAATCTCTTCCAAGAGCTTGATGCGGTTATCCATCTGCTTATGGAAGCCAAGCATTGGGGATTGAGAAAGAGTAGAAATGCCACAAGCCTCAATAGCAGGTTTAAGAGCCTCGATACGCTCAAAGTCAAAATAATCAGCGTGACATTCAGAAGGGT
```

This fits into the longer range (397)

When blasted, it hit PhiX.

[See attached picture](https://bytebucket.org/arhodes7/mouse_lung_vg4_tcr/raw/496fc2400b41daf2382922f1a57f3087b920077e/PhiX_Blast.png?token=fcfb936a073d31c9afdff18d2a1a597706e492a6)

So, PhiX reads were not removed by the MiSeq sequencer.

##PhiX reads were removed from trimmed reads

There is a program called [SMALT](http://www.sanger.ac.uk/science/tools/smalt-0), at Sanger Sequencing that can remove reads for particular genomes.  This was recommended by a [google group](https://groups.google.com/forum/#!topic/qiime-forum/yCQtm45tnDY) for removing PhiX from MiSeq reads.

I downloaded the PhiX genome from [NCBI](https://www.ncbi.nlm.nih.gov/nucleotide/NC_001422).

```bash

    smalt index -k 14 -s 8 PhiX PhiX.fasta
    smalt map -n 12 -o mapped.sam PhiX forward_paired.fq reverse_paired.fq
    samtools view -S -f4 mapped.sam > unmapped.sam
    cat unmapped.sam | grep -v ^@ | awk 'NR%2==1 {print "@"$1"\n"$10"\n+\n"$11}' > unmapped.forward.fastq
    cat unmapped.sam | grep -v ^@ | awk 'NR%2==0 {print "@"$1"\n"$10"\n+\n"$11}' > unmapped.reverse.fastq

#Check number of reads before combination
    grep "^@M02" unmapped.forward.fastq | wc -l
    11284829
    grep "^@M02" unmapped.reverse.fastq | wc -l
    11284828
```


##Pandaseq was rerun with a minimum length of 100.  No maximum length was set.

```bash

    pandaseq -l 100 -f forward_paired.fq -r reverse_paired.fq -T 12 -B -N -w combined.2ndattempt.fa

#Check combined reads
    grep ">" combined.2ndattempt.fa | wc -l
    11186764

#Check read distribution

    awk '/^>/ {print; next; } { seqlen = length($0); print seqlen}' combined.2ndattempt.fa | grep -v ">" | sort | uniq -c | sort -nk2

#Read length between 100 and 500
#Mean distribution peaks at 282, but covers a huge range.

```

So there are probably other contaminants in the data leading to a skewed distribution.  
This is where being told the anticpated insert size would be helpful, then reads could be pre-screened based on that.

##Biological contamination

Even though most of the PhiX was removed, fragments of PhiX and other bacteria (environmental contamination) are still in our reads. 

```
    grep -v CCAGGGTTTTCCCAGTCACGAC test.fa | head
```

>M02840:176:000000000-BLLHM:1:1101:16508:1371:1
TGTTATAGATATTCAAATAACCCTGAAACAAATGCTTAGGGATTTTATTGGTATCAGGGTTAATCGTGCCAAGAAAAGCGGCATGGTCAATATAACCAGTAGTGTTAACAGTCGGGAGAGGAGTGGCATTAACACCATCCTTCATGAACTTAATCCACTGTTCACCATAAACGTGACGATGAGGGACATAAAAAGTAAAAATGTCTACAGTAGAGTCAATAGCAAGGCCACGACGCAATGGAGAAAGACGGAGAGCGCCAACGGCGTCCATCTCGAAGGAGTCGCCAGCGATAACCGGAGTAGTTGAAATGGTAATAAGACGACC

Blasted to: Synthetic Enterobacteria phage CryptX174, complete genome
Sequence ID: MF426915.1Length: 5386Number of Matches: 1

1100 bp long, so that helps.  Also, no primers.

However, this is another case where knowing the desired insert size could save time.


Many of the issues actually had to do with primers.

##Problems with Primers

Primer-dimer or the 3rd reaction primer brings the 2nd reaction primer into the read.

```
    grep GCAGAGATAAGC combined.2ndattempt.fa | grep TGAAC | head | grep TGAAC
    
    grep AGATAAGGAGTACAAGAAAATGGA ../mouse_lung_vg4_tcr/Primer_Set.txt
```


2mouseGV4	CCAGGGTTTTCCCAGTCACGACNNCTTAGATAAGGAGTACAAGAAAATGGA


```
    grep M02840:176:000000000-BLLHM:1:1101:16458:2829:1 -A1 ../combined.2ndattempt.fa | grep AGATAAGGAGTACAAGAAAATGGA --color

```

AGATCCTGATGTTG**CCAGGGTTTTCCCAGTCACGAC**GGCTTAGATAAGGAGTACAAGAAAATGGAGGCAAGTAAAAATCCTAGTGCTTCTACATCGATATTGACAATATATTCCTTGGAGGAAGAAGACGAAGCTATCTACTACTGTTCCTGCGGCTTATATAGCCCAGGTTTTCACAAGGTATTTGCAGAAGGAACTAAGCTCATAGTAATTCCCTCTGACAAAAGGCTTGATGCAGACATTTCCCCCAAGCCCACTATTTTCCTTCCTTCTGTTGCTGAAACAAATCACGTCGA

```
    grep M02840:176:000000000-BLLHM:1:1101:16458:2829:1 -A1 P9G8_Vg4_Mouse_9_Sort_4_M_FBAR_ATCCTGATGTTG_RBAR_GACGT.fasta | grep CTTAGATAAGGAGTACAAGAAAATGGA --color

```

TTAGATAAGGAGTACAAGAAAATGGAGGCAAGTAAAAATCCTAGTG**CTTCTACATCGATATTGACAATATATTCCTTGGA**GGAAGAAGACGAAGCTATCTACTACTGTTCCTGCGGCTTATATAGCCCAGGTTTTCACAAGGTATTTGCAGAAGGAACTAAGCTCATAGTAATTCCCTCTGACAAAAGGCTTGATGCAGACATTTCCCCCAAGCCCACTATTTTCCTTCCTTCTGTTGCTGAAACAAATC



This is a problem, because we cannot strip the primers using a single position in the read.


This sequence is longer, maybe that could be used to detect? But that may get rid of true inserts.
Possibly screening resulting fasta files by primer?  Or just using mode to remove sequences within a smaller range, say 10 bp plus or minus instead of 20?

Or just reject longest sequences before alignment?

Problem:  We may be rejecting true biological information.

```
egrep -e "CCAGGGTTTTCCCAGTCACGACG[CGTAN]{0,2}GATTTTCTGTGAAGCACAGCAAG" ../combined.2ndattempt.fa | head | egrep -e "CCAGGGTTTTCCCAGTCACGACG[CGTAN]{0,2}GATTTTCTGTGAAGCACAGCAAG"

egrep -e "CCAGGGTTTTCCCAGTCACGACG[CGTAN]{0,2}GATTTTCTGTGAAGCACAGCAAG" ../combined.2ndattempt.fa | wc -l

#1097463

wc -l ../combined.2ndattempt.fa

#22373528 ../combined.2ndattempt.fa

```
This would remove about 5% of the reads.  Not a big loss.

#Primer Degeneracy

The primers changed sequence enough that not one sequence could be used to search and remove them.  Also, upon searching, not enough uniq sequence was found in long enough stretches to allow for a direct search using primers.

CC A GGGT T TTCCC A G T   C A CG A C  
CC A GGGT T TTCCC G G T   C A CG A C  
CC A GGGT C TTCCC A G T   C A CG A C  
CC G GGGT T TTCCC A G T   C A CG A C  
CC A GGGT T TTCCC G G T   C A CG A C  
CC G GGGT    TTCC C G G T C A CG A C  
CC A GGGT T TTCCC A G   T A A CG A C  
CC G GGGT    TTCC C G G T C A CG A C  
CC A GGGT    TTCC C A G T C A CG A C  
CC A GGGT T TTCCC A C   T C A CG A C  
CC A GGGT T TTCCC A G   T C A CG C C  
CC A GGGT T TTCCC A G   T C A  G A C  
CC A GGGT T TTCCC A G   T C G CG A C  

```
    awk '/CC[CGAT]{1}GGGT[CGAT]{0,1}TTCCC[CGTA]{2}T[CGTA]{1}A[C]{0,1}[GCTA]{1,2}C/' test.fa.size.selected | wc -l
#5971
    awk '/CC[CGAT]{1}GGGT[CGAT]{1}TTCCC[CGTA]{1}GTCACGAC/' test.fa.size.selected | wc -l
#5897
    awk '/CCAGGGTTTTCCCAGTCACGAC/' test.fa.size.selected | wc -l
#5860
    awk '!/CCAGGGTTTTCCCAGTCACGAC/' test.fa.size.selected | wc -l
#463

```

About 8% of the data would be removed if only exact primers were used for the search.


##Primers are not at the end of the sequence

In 99% of the projects, sequence comes off the sequencer already demultiplexed and with the primers on the end of the reads.

This method does not do that.  It has primers inside the barcodes.

If the primers are left in, it is not possible to cluster the data efficiently.

The ideal sequence in this case would come off the sequencer like this:

barcode1 primer1 insert primer2 barcode2

If all of the primers were the same size then we could just subtract them out based on position.

However, this project has several primer sizes, so it is not possible to choose one position for stripping them.


```
head primer_hits.txt
```

M02840:176:000000000-BLLHM:1:1101:15793:1372:1	15	66	+  
M02840:176:000000000-BLLHM:1:1101:16219:1372:1	15	62	+  
M02840:176:000000000-BLLHM:1:1101:15167:1374:1	15	62	+  
M02840:176:000000000-BLLHM:1:1101:14622:1378:1	15	62	+  
M02840:176:000000000-BLLHM:1:1101:16367:1376:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:15440:1378:1	15	62	+  
M02840:176:000000000-BLLHM:1:1101:15052:1368:1	15	62	+  
M02840:176:000000000-BLLHM:1:1101:16810:1378:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:16029:1380:1	15	62	+  
M02840:176:000000000-BLLHM:1:1101:14994:1381:1	15	65	+  


```
grep -v 62 primer_hits.txt | head
```

M02840:176:000000000-BLLHM:1:1101:15793:1372:1	15	66	+  
M02840:176:000000000-BLLHM:1:1101:16367:1376:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:16810:1378:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:14994:1381:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:14920:1366:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:16736:1412:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:15090:1365:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:17112:1437:1	15	65	+  
M02840:176:000000000-BLLHM:1:1101:16701:1381:1	13	60	+  
M02840:176:000000000-BLLHM:1:1101:15540:1384:1	15	63	+  


```
grep -v 15 primer_hits.txt | wc -l
#1796203
```

The different sized primers were designed to find different types of sequence, so using size selection would eliminate that diversity.

Using the 15,62 settings would remove 21% of the sequences that had legitimate primers, and which contained diverse biological data - the kind you are looking for in this project.

2mouseDV1	CCAGGGTTTTCCCAGTCACGACNNCGCTAAGCTGGATAAGAAAATGCAG  
2mouseDV2	CCAGGGTTTTCCCAGTCACGACNNCTCTGTGAACTTCCAGAAAGCAGC  
2mouseDV4	CCAGGGTTTTCCCAGTCACGACNNCCTCAAAGGGAAAATTAACATTTCAAA  
2mouseDV5	CCAGGGTTTTCCCAGTCACGACNNCGATTTTCTGTGAAGCACAGCAAG  
2mouseDV6	CCAGGGTTTTCCCAGTCACGACNNCTAYTCTGTAGTCTTCCAGAAATCA  
2mouseDV7	CCAGGGTTTTCCCAGTCACGACNNGTMCAATCCTTCTGGGACAAAGCA  
2mouseDV8	CCAGGGTTTTCCCAGTCACGACNNGATTCACAATCTTCTTCAATAAAAGGGA  
2mouseDV9	CCAGGGTTTTCCCAGTCACGACNNGGAAGCAGCAGAGGKTTTGAAGC  
2mouseDV10	CCAGGGTTTTCCCAGTCACGACNNGGGAGGCTAAAGTCAGCATTTGAT  
2mouseDV11	CCAGGGTTTTCCCAGTCACGACNNGGTCATTATTCTCTGAACTTTCAGAAG  
2mouseDV12	CCAGGGTTTTCCCAGTCACGACNNGGCTATTGCCTCTGACAGAAAGT  
2mouseGV1-3	CCAGGGTTTTCCCAGTCACGACNNACAARAAAATTGAAGCAAGTAAAGATTTT  
2mouseGV4	CCAGGGTTTTCCCAGTCACGACNNCTTAGATAAGGAGTACAAGAAAATGGA  
2mouseGV5	CCAGGGTTTTCCCAGTCACGACNNCCACTCCCGCTTGGAAATTGATGA  
2mouseGV6	CCAGGGTTTTCCCAGTCACGACNNGTGACGAAAGATATGAGGCAAGGA  
2mouseGV7	CCAGGGTTTTCCCAGTCACGACNNCATGTTTATGAAGGCCCGGACAAGA  

```
 grep "\sCCAGGGTTTTCCCAGTCACGAC" Primer_Set.txt | awk '{print $2}' | while read line; do echo $line| wc -c ; done
```
50  
49  
52  
49  
50  
49  
53  
48  
49  
52  
48  
54  
52  
49  
49  
50  


##Primer Problems Require Computer intensive code 

JOBS CURRENTLY RUNNING.

822 JOBS X 1.5 HOURS EACH /20 MAX RUNNING AT ONE TIME = 61 HOURS TO STRIP PRIMERS, AND WE HAVEN'T EVEN GOTTEN TO DEMULTIPLEXING YET

17182150.moab-mgmt1.cluster SPLITPS2         arhodes         00:31:38 R bsrq  
17182148.moab-mgmt1.cluster SPLITPS2         arhodes         00:32:00 R bsrq  
17182149.moab-mgmt1.cluster SPLITPS2         arhodes         00:31:44 R bsrq  
17182230.moab-mgmt1.cluster SPLITPS2         arhodes         00:36:43 R bsrq  
17187129.moab-mgmt1.cluster SPLITPS8         arhodes         02:06:34 C poolmemq  
17187130.moab-mgmt1.cluster SPLITPS8         arhodes         02:06:36 C poolmemq  
17187131.moab-mgmt1.cluster SPLITPS8         arhodes         01:52:36 R poolmemq  
17187132.moab-mgmt1.cluster SPLITPS8         arhodes         00:57:11 R poolmemq  
17187133.moab-mgmt1.cluster SPLITPS8         arhodes         00:57:01 R poolmemq  
17187134.moab-mgmt1.cluster SPLITPS8         arhodes         00:56:58 R poolmemq  
17187135.moab-mgmt1.cluster SPLITPS8         arhodes         00:56:35 R poolmemq  
17187136.moab-mgmt1.cluster SPLITPS8         arhodes         00:56:30 R poolmemq  
17187137.moab-mgmt1.cluster SPLITPS8         arhodes         00:55:55 R poolmemq  
17187138.moab-mgmt1.cluster SPLITPS8         arhodes         00:46:21 R poolmemq  
17187139.moab-mgmt1.cluster SPLITPS8         arhodes         00:46:23 R poolmemq  
17187140.moab-mgmt1.cluster SPLITPS8         arhodes         00:46:42 R poolmemq  
17187141.moab-mgmt1.cluster SPLITPS8         arhodes         00:45:31 R poolmemq  
17187142.moab-mgmt1.cluster SPLITPS8         arhodes         00:42:05 R poolmemq  


(c)2018 Adelaide.Rhodes@uvm.edu








